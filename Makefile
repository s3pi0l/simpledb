sdb: simpledb.c
	@echo "Compiling SimpleDB..."
	gcc simpledb.c -o sdb
	@echo "Completed compilation, making executable 'sdb'"
	@chmod +x sdb
	@echo "Finished."

clean:
	@echo "Removing executables..."
	@rm sdb
