#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

typedef struct {
    char *command;
    size_t command_length;
    ssize_t input_length;
} InputBuffer;

InputBuffer *new_input_buffer(void){
    InputBuffer *input_buffer = (InputBuffer *) malloc(sizeof(InputBuffer));
    input_buffer->command = NULL;
    input_buffer->command_length = 0;
    input_buffer->input_length = 0;
    return input_buffer;
}

void print_prompt(void) {
    printf("%s", "sdb ~ ");
}

void get_input(InputBuffer *input_buffer){
    size_t bytes_read = getline(&(input_buffer->command), &(input_buffer->command_length) , stdin);
    if (bytes_read <= 0) {
        printf("%s\n", "Error in reading the input!");
        exit (EXIT_FAILURE);
    }
    input_buffer->input_length = bytes_read - 1;
    input_buffer->command[bytes_read - 1] = 0;
}

void close_input_buffer(InputBuffer *input_buffer) {
    free(input_buffer->command);
    free(input_buffer);
}

int main(int argc, char *argv[]){
    InputBuffer *input_buffer = new_input_buffer();
    while (true) {
        print_prompt();
        get_input(input_buffer);
        
        if (strcmp(input_buffer->command, "/exit") == 0) {
            printf("%s\n", "Exiting...");
            close_input_buffer(input_buffer);
            exit (EXIT_SUCCESS);
        } else {
            printf("%s %s\n", "Invalid command: ", input_buffer->command);
        }
    }
}
